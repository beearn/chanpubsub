package chanpubsub

import "gitlab.com/beearn/entity"

type PublishSubscriber interface {
	Publish(topic string, msg *entity.Message) error
	Subscribe(topic string) <-chan *entity.Message
}
