package chanpubsub

import (
	"fmt"
	"gitlab.com/beearn/entity"
	"sync"
	"time"
)

type MsgBroker struct {
	topics map[string][]chan *entity.Message
	sync.Mutex
}

func NewMessageBroker() PublishSubscriber {
	n := &MsgBroker{}

	n.topics = make(map[string][]chan *entity.Message)

	return n
}

// Publish publishes a message to a topic.
func (n *MsgBroker) Publish(topic string, msg *entity.Message) error {
	if n.topics[topic] == nil {

		return fmt.Errorf("topic not found %s", topic)
	}
	n.Lock()
	for _, c := range n.topics[topic] {
		msg.LastUpdate = time.Now()
		msg.Topic = topic
		c <- msg
	}
	n.Unlock()

	return nil
}

// Sub subscribes to a topic.
func (n *MsgBroker) Subscribe(topic string) <-chan *entity.Message {
	n.Lock()
	c := make(chan *entity.Message, 1000)
	n.topics[topic] = append(n.topics[topic], c)
	n.Unlock()

	return c
}
